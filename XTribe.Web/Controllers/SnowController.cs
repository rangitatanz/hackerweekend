﻿using System.Web.Mvc;
using SampleMembershipSite.ViewModels;

namespace SampleMembershipSite.Controllers
{
	public class SnowController : BaseController
	{
		//
		// GET: /Snow/

		public ActionResult Index()
		{
			return View( new SnowHomepageViewModel() );
		}

		public ViewResult Maps()
		{
			return View();
		}
	}

	public class DisplayUnit
	{
		public string ImageURL { get; set; }
		public string HomepageImageURL
		{
			get { return ImageURL + "?height=300&width=300&mode=crop"; }
		}
		public string Name { get; set; }
	}
}