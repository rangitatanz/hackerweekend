﻿using System.Threading;
using System.Web;
using System.Web.Mvc;
using Common.Culture;

namespace SampleMembershipSite.Controllers
{
	public abstract class BaseController : Controller
	{
		protected override void ExecuteCore()
		{
			string cultureName = null;
			// Attempt to read the culture cookie from Request
			HttpCookie cultureCookie = Request.Cookies["_culture"];
			if( cultureCookie != null )
				cultureName = cultureCookie.Value;
			else
				cultureName = Request.UserLanguages[0]; // obtain it from HTTP header AcceptLanguages

			// Validate culture name
			cultureName = CultureProvider.GetImplementedCulture( cultureName ); // This is safe


			// Modify current thread's cultures            
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo( cultureName );
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

			base.ExecuteCore();
		}

	}
}
