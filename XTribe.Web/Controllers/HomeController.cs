﻿
using System.Web.Mvc;

namespace SampleMembershipSite.Controllers
{
	public class HomeController : BaseController
	{
        public ActionResult Index() {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About() {
            return View();
        }
    }
}
