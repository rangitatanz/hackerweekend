﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;

namespace SampleMembershipSite.ViewHelpers
{

	public static class HtmlHelpers
	{
		public const string MISSING_RESOURCE_KEY = "Missing resource: ";

		#region Public Methods

		public static string Translate( this HtmlHelper helper, string resourceKey )
		{
			return Translate( helper, resourceKey, Thread.CurrentThread.CurrentUICulture );
		}

		public static string Translate( this HtmlHelper helper, string resourceKey, CultureInfo culture )
		{
			string resourceObject = Resources.Resources.ResourceManager.GetString( resourceKey, culture );
			string translatedString = resourceObject ?? (MISSING_RESOURCE_KEY + String.Format( "{0}", resourceKey ));

			return translatedString;
		}

		public static string Translate( this HtmlHelper helper, string resourceKey, Dictionary<string, string> replacements )
		{
			return Translate( helper, resourceKey, Thread.CurrentThread.CurrentUICulture, replacements );
		}

		public static string Translate( this HtmlHelper helper, string resourceKey, CultureInfo culture, Dictionary<string, string> replacements )
		{
			string translatedString = helper.Translate( resourceKey, culture );

			translatedString = UpdatePlaceholders( helper, translatedString, replacements );

			return translatedString;
		}

		public static string Translate( this HtmlHelper helper, string resourceKey, params string[] replacements )
		{
			return Translate( helper, resourceKey, Thread.CurrentThread.CurrentUICulture, replacements );
		}

		public static string Translate( this HtmlHelper helper, string resourceKey, CultureInfo culture, params string[] replacements )
		{
			return string.Format( Translate( helper, resourceKey, culture ), replacements );
		}

		public static string UpdatePlaceholders( this HtmlHelper helper, string text, Dictionary<string, string> replacements )
		{
			var re = new Regex( @"@@(\w+)@@" );
			return re.Replace( text, match =>
			{
				string placeholderKey = match.Groups[1].Value;
				string placeholderValue;
				return replacements.TryGetValue( placeholderKey, out placeholderValue )
						   ? placeholderValue
						   : match.Value;
			} );
		}

		#endregion
	}
}