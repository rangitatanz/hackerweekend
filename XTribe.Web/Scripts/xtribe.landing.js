﻿/* File Created: May 26, 2012 */

$(function () {

    var activeEl = $('.descriptions_default');
    var timeout = null;
    var ThumbView = Backbone.View.extend({
        initialize: function () {
            var base = this;
            $(this.model.ThumbImage).popover({
                placement: 'top'
            });
        }
    });
    function wheretoplace() {
        var width = window.innerWidth;
        if (width < 500) return 'below';
        return 'left';
    }
    var LandingView = Backbone.View.extend({
        el: $("#landing-content"),
        events: {
            "click #requestinvite": "openContactForm",
            "click #closeContactform": "closeContactForm"
        },
        initialize: function () {
            var wave = new ThumbView({
                model: {
                    ThumbImage: '.thumb_wave',
                    Description: '.descriptions_wave'
                }
            });
            var mountain = new ThumbView({
                model: {
                    ThumbImage: '.thumb_mountain',
                    Description: '.descriptions_mountain'
                }
            });
            var wheel = new ThumbView({
                model: {
                    ThumbImage: '.thumb_Wheel',
                    Description: '.descriptions_wheel'
                }
            });
            $('#contactForm').validate({
                errorClass: 'error',
                validClass: 'success',
                errorElement: 'span',
                highlight: function (element, errorClass, validClass) {
                    $(element)
                        .parents("div[class='control-group']")
                        .addClass(errorClass)
                        .removeClass(validClass);
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parents(".error").removeClass(errorClass).addClass(validClass);
                },
                submitHandler: this.sendEmail
            });
        },
        sendEmail: function () {
            $.post('/Landing/AddContact', {
                emailAddress: $('#emailAddress').val()
            }, function () {
                $('#contact-formContent2').hide('normal', function () {
                    $('#contact-formContent3').show();
                });
            });
        },
        openContactForm: function () {
            $('#contact-form').slideDown('fast', function () {
                $('#signupbutton').slideUp('fast');
            })
        },
        closeContactForm: function () {
            $('#contact-form').hide();
        }
    });

    var App = new LandingView;
    window.fbAsyncInit = function () {
        FB.init({ 
            appId: '323177461089690',
            status: true,
            cookie: false, 
            xfbml: true,
            channelUrl: 'http://xtribe.co/facebook-channel.html'
        });
    };
    $(document).ready(function () {
        if (document.getElementById('fb-root') != undefined) {
            var e = document.createElement('script');
            e.type = 'text/javascript';
            e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
            e.async = true;
            document.getElementById('fb-root').appendChild(e);
        }
    });
});

function afterFacebookConnect() {
    FB.getLoginStatus(function (response) {
        if (response.authResponse) {
            $.post('/Landing/AddContact', {
                accessToken: response.authResponse.accessToken
            }, function () {
                $('#contact-formContent2').hide('normal', function () {
                    $('#contact-formContent3').show();
                });
            });
        }
    });
};