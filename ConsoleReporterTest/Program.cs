﻿

using System;
using System.Threading;

using SignalR.Client;
using SignalR.Hosting.Memory;

using XTribe.Communication.Connection;

namespace ConsoleReporterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new MemoryHost();
            host.MapConnection<SendMessageConnection>("/echo");

            var connection = new Connection("http://dev.xtribe.co/operations/echo");

            connection.Received += data =>
            {
                Console.WriteLine(data);
            };

            connection.StateChanged += change =>
            {
                Console.WriteLine(change.OldState + " => " + change.NewState);
            };

            connection.Start().Wait();

            ThreadPool.QueueUserWorkItem(_ =>
            {
                try
                {
                    while (true)
                    {
                        connection.Send("Sarah Smells At " + DateTime.Now.ToString());

                        Thread.Sleep(2000);
                    }
                }
                catch
                {

                }
            });
        }
    }
}
