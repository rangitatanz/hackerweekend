namespace Common.Data
{
    public interface IDataAccessConfiguration
    {
        string ConnectionString { get; }
    }
}