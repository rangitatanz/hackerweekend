﻿(function ($) {
    var TO = false;
    var $container;
    var map;
    var $coords;
    function SnowMap(elem, options) {
        this.init(elem, options);
    }

    SnowMap.prototype.init = function (elem, options) {
        $container = $(elem);

        buildMap();
        mapResize();

        //        $(window).resize(function () {
        //            if (TO !== false) {
        //                clearTimeout(TO);
        //            }
        //            TO = setTimeout(imageResize, 200); //200 is time in miliseconds
        //        });
    };

    /**
    * Extends JQuery to create an instance of ExtendedDatePicker for each provided element.
    */
    $.fn.snowMap = function (options) {
        return this.each(function () {
            // Instantiate a new extendedDatePicker, storing the instance in the element's data cache
            $.data(this, "snowMap", new SnowMap(this, options));
        });
    };

    $.fn.snowMap.setPointer = function (id, coords) {

        addElement(id, coords);

        return function () {

        };
    };

    $.fn.snowMap.getCoords = function () {
        return $coords;
    };

    var mapResize = function (map) {
        var header = $('#header');
        var footer = $('#footer');

        var windowHeight = $(window).height();
        if (footer.is(":visible")) {
            $container.height(windowHeight - footer.height());
        } else {
            $container.height(windowHeight);
        }

        if (map) {
            var center = map.getCenter();
            google.maps.event.trigger(map, 'resize');
            map.setCenter(center);
        }
    };
    function logMsg(msg) {
        log.append("<li>" + msg + "</li>");
    }
    var initialPositionMap = function (map) {
        var marker;
        watchID = navigator.geolocation.watchPosition(function (position) {
            $coords = position.coords;
            var coords = $coords;
            //			$("#lat").val(coords.latitude);
            //			$("#long").val(coords.longitude);
            //     
            //			$("#acc").val(coords.accuracy);
            //			$("#alt").val(coords.altitude);
            //			$("#altAcc").val(coords.altitudeAccuracy);
            //			$("#heading").val(coords.heading);
            //			$("#speed").val(coords.speed);
            //			$("#timestamp").val(coords.timestamp);
            var pos = new google.maps.LatLng(coords.latitude, coords.longitude);
            map.setCenter(pos, 13);
            map.panTo(pos);

            if (!marker) {
                var pinColor = "0000FF";
                var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                    new google.maps.Size(21, 34),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(10, 34));
                var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                    new google.maps.Size(40, 37),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(12, 35));
                marker = new google.maps.Marker({
                    map: map,
                    position: pos,
                    title: 'Location found using HTML5.',
                    draggable: false,
                    icon: pinImage,
                    shadow: pinShadow
                });
                window.log("You " + pos.lat() + " " + pos.lng());

            } else {
                marker.setPosition(pos);
            }

        }, function (e) {
            switch (e.code) {
                case 0:
                    // UNKNOWN_ERROR
                    logMsg("The application has encountered an unknown error while trying to determine your current location. Details: " + e.message);
                    break;
                case 1:
                    // PERMISSION_DENIED
                    logMsg("You chose not to allow this application access to your location.");
                    break;
                case 2:
                    // POSITION_UNAVAILABLE
                    logMsg("The application was unable to determine your location.");
                    break;
                case 3:
                    // TIMEOUT
                    logMsg("The request to determine your location has timed out.");
                    break;
            }
        });
    };
    var buildMap = function () {
        /*
        Build list of map types.
        You can also use var mapTypeIds = ["roadmap", "satellite", "hybrid", "terrain", "OSM"]
        but static lists sucks when google updates the default list of map types.
        */
        // var element = document.getElementById('map');
        var element = $container.context;
        var mapTypeIds = [];
        for (var type in google.maps.MapTypeId) {
            mapTypeIds.push(google.maps.MapTypeId[type]);
        }
        mapTypeIds.push("OSM");

        map = new google.maps.Map(element, {
            center: new google.maps.LatLng(48.1391265, 11.580186300000037),
            zoom: 13,
            mapTypeId: "OSM",
            mapTypeControlOptions: {
                mapTypeIds: mapTypeIds
            }
        });

        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function (coord, zoom) {
                return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));

        $(window).resize(function () {
            mapResize(map);
        });

        initialPositionMap(map);
    };
    var markerArray = new Array();
    var addElement = function (id, coords) {
        var pinColor = "FFFF00";
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0, 0),
            new google.maps.Point(10, 34));
        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
            new google.maps.Size(40, 37),
            new google.maps.Point(0, 0),
            new google.maps.Point(12, 35));

        var lat = coords.lat, lng = coords.lng;
        var i = 0;
        var newPerson = new google.maps.LatLng(lat, lng);
        var set = false;
        for (i = 0; i < markerArray.length; i++) {
            var obj = markerArray[i];
            if (obj.id == id) {
                var pos = obj.marker.getPosition();

                if (pos.Xa != lat || pos.Ya != lng) {
                    window.log("person " + obj.id + " moved to " + newPerson.lat() + " " + newPerson.lng());
                }

                obj.marker.setPosition(newPerson);
                set = true;
            }
        }

        if (!set) {
            window.log("new person " + newPerson.lat() + " " + newPerson.lng());

            markerArray.push({
                id: id,
                marker: new google.maps.Marker({
                    map: map,
                    position: newPerson,
                    title: i.toString(),
                    draggable: false,
                    icon: pinImage,
                    shadow: pinShadow
                })
            });
        }
    };
    var throbID;
    var throb = function () {
        if (throbID !== false) {
            clearTimeout(throbID);
        }
        if (markerArray) {
            for (var i = 0; i < 5; i++) {
                var marker = markerArray[i];
                var pos = marker.getPosition();
                var r = Math.random();
                var lat, lng;
                var factor = r * 1 / 10000;

                if (r < 0.5) {
                    lat = pos.lat() + factor;
                }
                else {
                    lat = pos.lat() - factor;
                }

                r = Math.random();
                factor = r * 1 / 10000;
                if (r < 0.5) {
                    lng = pos.lng() + factor;
                }
                else {
                    lng = pos.lng() - factor;
                }
                var newPerson = new google.maps.LatLng(lat, lng);
                marker.setPosition(newPerson);
            }
        }
        throbID = setTimeout(throb, 1000);
    };


})(jQuery);