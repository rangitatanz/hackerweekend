﻿using System.Web.Mvc;

namespace SampleMembershipSite.Controllers
{
	public class SearchController : BaseController
    {
        //
        // GET: /Search/
        // TODO Combine
        public ActionResult Today(string searchString)
        {
            ViewBag.SearchString = searchString;

            return View("Search");
        }

        public ActionResult Tomorrow(string searchString)
        {
            ViewBag.SearchString = searchString;

            return View("Search");
        }

    }
}
