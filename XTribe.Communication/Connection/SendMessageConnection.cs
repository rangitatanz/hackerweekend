﻿// -----------------------------------------------------------------------
// <copyright file="SendMessageConnection.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using SignalR;

namespace XTribe.Communication.Connection
{
    public class SendMessageConnection : PersistentConnection
    {
        protected override Task OnReceivedAsync(IRequest request, string connectionId, string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                dynamic d = JObject.Parse(data);

                return Connection.Broadcast(new { coords = new { lng = d.lng, lat = d.lat }, connectionId = connectionId });
            }

            return null;
        }
    }
}
