namespace Common.Data
{
    public class DataAccessConfiguration : IDataAccessConfiguration
    {
        public string ConnectionString
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Xtribe.Dev"].ConnectionString;
            }
        }
    }
}