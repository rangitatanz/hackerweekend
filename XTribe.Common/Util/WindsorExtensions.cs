﻿// -----------------------------------------------------------------------
// <copyright file="WindsorExtensions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Async;

using Castle.Core;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace Common.Util
{
    public static class WindsorExtensions
    {
        #region Public Methods

        public static bool IsController(Type type)
        {
            return type != null &&
                    type.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase) &&
                    !type.IsAbstract &&
                    IsControllerType(type);
        }

        private static bool IsControllerType(Type type)
        {
            return typeof(IController).IsAssignableFrom(type) || typeof(IAsyncController).IsAssignableFrom(type);
        }

        public static IWindsorContainer RegisterController<T>(this IWindsorContainer container) where T : IController
        {
            container.RegisterControllers(typeof(T));

            return container;
        }

        public static IWindsorContainer RegisterControllers(
            this IWindsorContainer container, params Type[] controllerTypes)
        {
            foreach (Type type in controllerTypes)
            {
                if (IsController(type))
                {
                    container.Register(Component.For(type).Named(type.FullName).LifeStyle.Is(LifestyleType.Transient));
                }
            }

            return container;
        }

        public static IWindsorContainer RegisterControllers(
            this IWindsorContainer container, params Assembly[] assemblies)
        {
            foreach (Assembly assembly in assemblies)
            {
                container.RegisterControllers(assembly.GetExportedTypes());
            }

            return container;
        }

        #endregion
    }
}
