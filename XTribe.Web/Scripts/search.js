﻿$(function () {
    var toggle = function () {
        $("#searchContainer").show();
        $("#findSkiBuddy").hide();
        $("#txtSearch").focus();
    };

    var searchToday = function () {
        var searchString = $("#txtSearch").val();
        document.location = '/Search/Today/' + searchString;
    };

    $("#friendSearch").hover(function () {
        toggle();
    });

    $('#btnToday').click(function () {
        searchToday();
    });

    $('#btnTomorrow').click(function () {
        var searchString = $("#txtSearch").val();

        document.location = '/Search/Tomorrow/' + searchString;
    });

    $(document).keydown(function (e) {
        if (e.keyCode >= 48 && e.keyCode <= 90)
            toggle();
    });

    $("#txtSearch").keydown(function (e) {
        if (e.keyCode == 13) {
            searchToday();
        }
    });
});