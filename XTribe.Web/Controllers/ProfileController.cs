﻿using System.Web.Mvc;
using Common.Data;
using SampleMembershipSite.ViewModels;

namespace SampleMembershipSite.Controllers
{
    [Authorize]
	public class ProfileController : BaseController
    {
        private IProfileRepository _profileRepository;

        public ProfileController()
            : this(new DummyProfileRepository())
        {

        }

        public ProfileController(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Wall()
        {
            var userprofile = _profileRepository.GetProfileByUserName(User.Identity.Name);

            return View(new ProfileViewModel(userprofile));
        }
    }
}
