namespace XTribe.Communication.Connection
{
    public interface IConnectionConfiguration
    {
        string LoggingServerURL { get; set; }

        string LoggingConnectionPath { get; set; }
    }

    public class ConnectionConfiguration : IConnectionConfiguration
    {
        public string LoggingServerURL { get; set; }

        public string LoggingConnectionPath
        {
            get;
            set;
        }
    }
}