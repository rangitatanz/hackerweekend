﻿namespace Common.Models
{
    public class UserProfile
    {
        public string ImageURL { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public UserLevel Level { get; set; }

        public string EmailAddres { get; set; }

        public string Twitter { get; set; }

        public string FacebookId { get; set; }

        public string Locale { get; set; }

        public string FacebookLinkURL
        {
            get;
            set;
        }
    }

    public enum UserLevel
    {
        Green = 1,
        Blue = 2,
        Red = 3,
        Black = 4,
        Yellow = 5,
        BlackDiamond = 6,
        BlackDoubleDiamond = 7
    }
}