﻿// -----------------------------------------------------------------------
// <copyright file="IDBHealthcheck.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Common.Data
{
    public interface IDatabaseHealthCheck
    {
        /// <summary>
        /// Checks if the database is available
        /// </summary>
        /// <returns></returns>
        bool IsDatabaseAvailable();
    }
}
