﻿using System.Collections.Generic;
using SampleMembershipSite.Controllers;

namespace SampleMembershipSite.ViewModels
{
    public class SnowHomepageViewModel
    {
        public SnowHomepageViewModel()
        {
            var displayUnits = new List<DisplayUnit>();

            for(int i = 1; i <= 9; i++)
            {
                displayUnits.Add(new DisplayUnit()
                                     {
                                         ImageURL = "/Content/images/profiles/ski" + i + ".png",
                                         Name = "Ski " + i
                                     });
            }

            DisplayUnits = displayUnits;
        }

        public IEnumerable<DisplayUnit> DisplayUnits { get; set; }
    }
}