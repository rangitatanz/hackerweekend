﻿// -----------------------------------------------------------------------
// <copyright file="CommunicationInstaller.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using XTribe.Communication.Connection;

namespace XTribe.Communication.IoC
{
    public class PriceAlertsCommunicationWindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            ConnectionConfiguration configuration = new ConnectionConfiguration()
            {
                LoggingConnectionPath = "operations/echo",
                LoggingServerURL = "http://www.xtribe.co/"
            };
            container.Register(
                Component.For<IConnectionConfiguration>().Instance(configuration),
                Component.For<IConnection>()
                    .ImplementedBy<DefaultConnection>()
                    .LifeStyle.Singleton
                );
        }
    }
}
