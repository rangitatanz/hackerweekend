﻿
using System.Web.Mvc;

using Common.Data;

namespace SampleMembershipSite.Controllers
{
    public class SystemController : Controller
    {
        private IDatabaseHealthCheck _databaseHealthCheck;

        public SystemController(IDatabaseHealthCheck databaseHealthCheck)
        {
            _databaseHealthCheck = databaseHealthCheck;
        }

        public HttpStatusCodeResult HealthCheck()
        {
            if (!_databaseHealthCheck.IsDatabaseAvailable())
            {
                return new HttpStatusCodeResult(500, "Database Failed");
            }

            return new HttpStatusCodeResult(200, "Website Ok");
        }

    }
}
