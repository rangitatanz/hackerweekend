﻿// -----------------------------------------------------------------------
// <copyright file="WindsorContainerContext.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections;

using Castle.MicroKernel.Registration;
using Castle.Windsor;

using log4net;

namespace Common.Util
{

    public class WindsorContainerContext
    {
        private static IWindsorContainer _container;
        private static readonly object _sync = new object();
        private static readonly ILog _log = LogManager.GetLogger(typeof(WindsorContainerContext));

        /// <summary>
        /// Gets/sets the underlying windsor container
        /// </summary>
        public static IWindsorContainer Container
        {
            get
            {
                if (_container == null)
                {
                    createContainer();
                }

                return _container;
            }
            set { _container = value; }
        }

        /// <summary>
        /// Resolves the default implementation of <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        /// <summary>
        /// Resolves the default implementation of <typeparamref name="T" />.
        /// </summary>
        /// <param name="constructorArgs">List of constructor arguments.</param>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>(IDictionary constructorArgs)
        {
            return Container.Resolve<T>(constructorArgs);
        }

        /// <summary>
        /// Resolves the default implementation of <typeparamref name="T" />.
        /// </summary>
        /// <param name="anonymousObject">Anonymous object containing constructor arguments.</param>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>(object anonymousObject)
        {
            return Container.Resolve<T>(anonymousObject);
        }

        /// <summary>
        /// Resolves a specific implementation of <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>(string key)
        {
            return Container.Resolve<T>(key);
        }

        /// <summary>
        /// Resolves a specific implementation of <typeparamref name="T" />.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="constructorArgs">List of constructor arguments.</param>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>(string key, IDictionary constructorArgs)
        {
            return Container.Resolve<T>(key, constructorArgs);
        }

        /// <summary>
        /// Resolves a specific implementation of <typeparamref name="T" />.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="anonymousObject">Anonymous object containing constructor arguments.</param>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved concrete implementation.</returns>
        public T Resolve<T>(string key, object anonymousObject)
        {
            return Container.Resolve<T>(key, anonymousObject);
        }

        /// <summary>
        /// Resolves all implementations of <typeparamref name="T" />.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>All resolved implementations.</returns>
        public T[] ResolveAll<T>()
        {
            return Container.ResolveAll<T>();
        }

        /// <summary>
        /// Determines if a specific type is registered with the 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool CanResolve<T>()
        {
            return Container.Kernel.HasComponent(typeof(T));
        }

        /// <summary>
        /// Allows an instance of a type to be registered with the container as a singleton
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        public void RegisterInstance<T>(T instance) where T : class
        {
            Container.Register(Component.For<T>().Instance(instance));
        }

        /// <summary>
        /// Explcitly releases a component instance from the container
        /// </summary>
        /// <param name="instance"></param>
        public void Release(object instance)
        {
            Container.Release(instance);
        }



        /// <summary>
        /// Initialises the underlying container.
        /// </summary>
        /// <param name="installers">A list of 'installer' instances  that can add components to the container.</param>
        /// <remarks>
        /// Should be called once at application start up.
        /// </remarks>
        public static void InstallContainerComponents(params IWindsorInstaller[] installers)
        {
            try
            {
                lock (_sync)
                {
                    foreach (var installer in installers)
                    {
                        Container.Install(installer);
                    }
                }
            }
            catch (Exception exception)
            {
                _log.Error("Exception occured whilst installing WindsorContainer components", exception);
                throw;
            }
        }

        private static void createContainer()
        {
            try
            {
                lock (_sync)
                {
                    _container = new WindsorContainer();
                }
            }
            catch (Exception exception)
            {
                _log.Error("Exception occured whilst creating WindsorContainer", exception);
                throw;
            }
        }

    }
}
