using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Elmah;

namespace SampleMembershipSite.Controllers {
	public class LandingController : BaseController
	{
        public ActionResult Index() {
            return View();
        }

        public JsonResult AddContact(string emailAddress, string accessToken) {
            var smtp = new SmtpClient {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("www@xtribe.co", "MailServer01!")
            };

            string body = null;

            if (!string.IsNullOrEmpty(accessToken)) {
                var facebook = new Facebook.FacebookClient(accessToken);
                dynamic asd = facebook.Get("me");
                emailAddress = asd.email;
            }
            try {
                using (var message = new MailMessage("invites@xtribe.co", "alex@xtribe.co") {
                    Subject = "New Contact",
                    Body = "A user with the email address " + emailAddress + " requested an invite"
                }) {
                    smtp.Send(message);
                }
            } catch (Exception ex) {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            try {

                using (var message = new MailMessage("invites@xtribe.co", emailAddress) {
                    Subject = "Thanks for your Interest",
                    Body = "Hi there!\r\nOur services is nearing the completion of our private beta which means we will soon be opening up invites to the general public. We will notify you when this exciting new service goes live!\r\n"
                }) {
                    smtp.Send(message);
                }
            } catch (Exception ex) {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return new JsonResult() {
                Data = new {
                    isOK = true
                }
            };
        }
    }
}