using System.Data;
using System.Data.SqlClient;
using System.Linq;

using Dapper;

namespace Common.Data
{
    public class DatabaseHealthCheck : IDatabaseHealthCheck
    {
        private readonly IDataAccessConfiguration _dataAccessConfiguration;

        public DatabaseHealthCheck(IDataAccessConfiguration dataAccessConfiguration)
        {
            this._dataAccessConfiguration = dataAccessConfiguration;
        }

        public bool IsDatabaseAvailable()
        {
            const string SpName = "SystemIsOnline";

            using (var sqlConnection = new SqlConnection(this._dataAccessConfiguration.ConnectionString))
            {
                sqlConnection.Open();
                int result =
                    sqlConnection.Query<int>(SpName, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

                return (result == 1);
            }
        }
    }
}