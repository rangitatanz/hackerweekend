﻿// -----------------------------------------------------------------------
// <copyright file="IConnection.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace XTribe.Communication.Connection
{
    public interface IConnection
    {
        bool IsConnected { get; }
        void Send(string message);
        void Connect();
    }
}
