﻿using Common.Models;

namespace SampleMembershipSite.ViewModels
{
    public class ProfileViewModel
    {
        public UserProfile UserProfile
        {
            get;
            set;
        }

        public ProfileViewModel(UserProfile userProfile)
        {
            UserProfile = userProfile;
        }

        public string UserFullName
        {
            get { return string.Join(" ", UserProfile.FirstName, UserProfile.LastName); }
        }

        public string SmallImageURL
        {
            get { return string.Format("http://graph.facebook.com/{0}/picture", UserProfile.UserName); }
        }

        public string ProfileImageURL
        {
            get { return string.Format("http://graph.facebook.com/{0}/picture?type=large", UserProfile.UserName); }
        }
    }
}