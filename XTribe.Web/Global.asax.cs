﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

using Castle.Windsor;

using Common.Data;
using Common.Util;

using SignalR;

using StackExchange.Profiling;
using StackExchange.Profiling.MVCHelpers;

using XTribe.Communication.Connection;
using XTribe.Communication.IoC;

namespace SampleMembershipSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ProfilingActionFilter());
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapConnection<SendMessageConnection>("echo", "operations/echo/{*operation}");

            routes.MapRoute(
                "Search", // Route name
                "Search/{action}/{searchString}", // URL with parameters
                new { controller = "Search", action = "Index", searchString = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Landing", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            ConfigureContainer();

            DummyProfileRepository.CreateUserList();
        }

        private void ConfigureContainer()
        {
           

            WindsorContainerContext.InstallContainerComponents(new PriceAlertsCommunicationWindsorInstaller());
            IWindsorContainer windsorContainer = WindsorContainerContext.Container;
            windsorContainer.RegisterControllers(Assembly.GetExecutingAssembly());
            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(windsorContainer));
        }

        protected void Application_BeginRequest()
        {
            if (Request.IsLocal)
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_EndRequest()
        {
            if (Request.IsLocal)
            {
                MiniProfiler.Start();
            }
        }
    }
}