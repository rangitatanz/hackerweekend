﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Models;

namespace Common.Data
{
    public interface IProfileRepository
    {
        UserProfile GetProfileByUserName(string username);

        UserProfile CreateNewProfile(string userName, string emailAddress, string facebookKey, string firstName, string lastName, string locale, string facebookLinkURL);
    }

    public class DummyProfileRepository : IProfileRepository
    {
        public static List<UserProfile> UserProfiles { get; set; }

        public UserProfile GetProfileByUserName(string username)
        {
            return UserProfiles.FirstOrDefault(a => a.UserName == username);
        }

        public UserProfile CreateNewProfile(string userName, string emailAddress, string facebookKey, string firstName, string lastName, string locale, string facebookLinkURL)
        {
            UserProfile profile = new UserProfile();

            profile.EmailAddres = emailAddress;
            profile.FirstName = firstName;
            profile.LastName = lastName;
            profile.Gender = "Male";
            profile.ImageURL = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc4/261046_571467650_4607150_n.jpg";
            profile.Level = UserLevel.Green;
            profile.Twitter = "@dchristiansen";
            profile.FacebookId = facebookKey;
            profile.UserName = userName;
            profile.Locale = locale;
            profile.FacebookLinkURL = facebookLinkURL;

            UserProfiles.Add(profile);

            return profile;
        }

        public static void CreateUserList()
        {
            UserProfile profile = new UserProfile();

            profile.EmailAddres = "rangitatanz@gmail.com";
            profile.FirstName = "Dave";
            profile.LastName = "Walker";
            profile.Gender = "Male";
            profile.ImageURL = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc4/261046_571467650_4607150_n.jpg";
            profile.Level = UserLevel.Red;
            profile.Twitter = "@dchristiansen";
            profile.FacebookId = "571467650";
            profile.UserName = "rangitatanz";
            UserProfiles = new List<UserProfile>();

            UserProfiles.Add(profile);
        }
    }
}
