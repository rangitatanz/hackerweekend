﻿using System;

using ConnectionState = SignalR.Client.ConnectionState;

namespace XTribe.Communication.Connection
{

    public class DefaultConnection : IConnection
    {
        private readonly IConnectionConfiguration _connectionConfiguration;
        private SignalR.Client.Connection _connection;

        private string FullConnectionPath
        {
            get
            {
                return _connectionConfiguration.LoggingServerURL +
                       _connectionConfiguration.LoggingConnectionPath;
            }
        }

        public DefaultConnection(IConnectionConfiguration connectionConfiguration)
        {
            _connectionConfiguration = connectionConfiguration;
        }

        public void Connect()
        {
            if (!string.IsNullOrEmpty(FullConnectionPath))
            {
                _connection = new SignalR.Client.Connection(FullConnectionPath);

                _connection.StateChanged += change => Console.WriteLine(change.OldState + " => " + change.NewState);

                _connection.Start().Wait();
            }
        }

        public bool IsConnected
        {
            get
            {
                return _connection != null && _connection.State == ConnectionState.Connected;
            }
        }

        public void Send(string message)
        {
            if (IsConnected)
            {
                _connection.Send(message);
            }
        }
    }
}
